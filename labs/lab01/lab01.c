#include "pico/stdlib.h"

/**
 * @brief EXAMPLE - BLINK_C
 *        Simple example to initialise the built-in LED on
 *        the Raspberry Pi Pico and then flash it forever. 
 * 
 * @return int  Application return code (zero for success).
 */
int main() {

    // Specify the PIN number and sleep delay
    const uint LED_PIN   =  25;
    const uint LED_DELAY = 500;

    // Setup the LED pin as an output.
    gpio_init(LED_PIN);
    gpio_set_dir(LED_PIN, GPIO_OUT);

    // Do forever...
    while (true) {
        // set a parameter to control the LED on and off, initialize the parameter to 1 to continue
        int p=1;
        // Toggle the LED on and then sleep for delay period
        while(p==1){
            gpio_put(LED_PIN, p);
            sleep_ms(LED_DELAY);
            // Toggle the LED off
            p = 0;
        }
        // Toggle the LED off and then sleep for delay period
        while(p==0){
            gpio_put(LED_PIN, p);
            sleep_ms(LED_DELAY);
            // Toggle the LED on
            p = 1;
        }
    }

    // Should never get here due to infinite while-loop.
    return 0;

}
